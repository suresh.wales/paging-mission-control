import pandas as pd
from datetime import datetime, timezone
df = pd.read_csv("input.txt", sep = '|', header=None)
df.columns = ['timestamp','satellite-id','red-high-limit','yellow-high-limit','yellow-low-limit','red-low-limit','raw-value','component']
df = df.drop(['yellow-low-limit','yellow-high-limit'], axis=1)

# If 3 TSTAT > red high limit within a five minute interval
# If 3 BATT readings < red low limit within a five minute interval

satelite_list = list(set(df["satellite-id"].tolist()))
a_list = ['TSTAT','BATT']
output = []
for x in satelite_list:
    for y in a_list:
        tstat_alert = 0
        batt_alert = 0
        sattelite_df = df.query(f'`satellite-id` == {x} & `component` == "{y}"')
        sattelite_df.reset_index(drop=False, inplace=True)
        ts = sattelite_df[['timestamp']].iloc[0].timestamp
        dt_object = datetime.strptime(ts, "%Y%m%d %H:%M:%S.%f").strftime(f"%Y-%m-%dT%H:%M:%S.%f") [:-3]+'Z'

        for ind, row in sattelite_df.iterrows():
            val = row['raw-value']
            if 'TSTAT' in y:
                lev = row['red-high-limit']
                if val > lev:
                    tstat_alert +=1
            if 'BATT' in y:
                lev = row['red-low-limit']
                if val < lev:
                    batt_alert +=1
        if tstat_alert == 3:
            js = {
            "satelliteId": x,
            "severity": "RED HIGH",
            "component": "TSTAT",
            "timestamp": dt_object
            }
            output.append(js)

        if batt_alert == 3:
            js = {
            "satelliteId": x,
            "severity": "RED LOW",
            "component": "BATT",
            "timestamp": dt_object
            }
            output.append(js)


print(output)

